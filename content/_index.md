---
title: "Homepage"
date: 2022-09-26T15:12:59-03:00
draft: false
layout: "index.html"
---

### Sobre a franquia: 

Pokémon é uma série de jogos eletrônicos desenvolvidos pela Game Freak e publicados pela Nintendo como parte da franquia de mídia Pokémon. Lançado pela primeira vez em 1996 no Japão para o console Game Boy, a principal série de jogos de RPGs, que continuou em cada geração em portáteis da Nintendo.

Os jogos são geralmente lançados em pares, sendo que cada um contém pequenas variações em relação ao outro. Enquanto a série principal consiste em RPGs, os spin-off abrangem outros gêneros, como RPG de ação, quebra-cabeça e jogos virtuais para animais de estimação.

A partir de 24 de novembro de 2017 , mais de 300 milhões de jogos de Pokémon foram vendidos em todo o mundo, em 76 títulos. Isso faz de Pokémon a segunda franquia de jogos eletrônicos mais vendidas, atrás da própria franquia da Nintendo Mario.


{}
### Sobre o anime:

O anime de Pokemon mundialmente conhecido pelos fâs conta com mais de 1200 episódios nas suas séries principais, além de 23 filmes. A maioria da história do desenho foca na vida de Ash Ketchum, um treinador pokemon da cidade de Pallet, em sua jornada para se tornar um mestre Pokemon. Ao longo de sua jornada, Ash faz inúmeros amigos, humanos e Pokemons, os considerando como família.

Em especial, vale ressaltar o fiel companheiro de Ash que acabou se tornando uma das maiores marcas da empresa: o Pikachu. Presente durante todo anime, este Pokemon acabou se tornando um ícone da marca e porntanto, ele terá uma página única para ele neste site.

### Sobre os Jogos:

Sobre os jogos, ultimamente seus lançamentos são realizados anteriormente aos lançamentos de seus respectivos animes, portanto os jogos acabam sendo uma forma do público obter um primeiro contato com o novo universo que será desenvolvido na nova geração. Importante ressaltar que as história do jogo e do anime são diferentes, inclusive o próprio Ash não tem participação nos jogos. Mais informações sobre os jogos podem ser encontradas neste site.