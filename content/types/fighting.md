---
title: "Fighting Type"
date: 2022-10-08T12:23:52-03:00
draft: false
---



Os Pokemon Lutador são especialistas em ataques corpo a corpo, como socos e chutes. Pode-se dizer que eles são o oposto dos Pokémon do tipo Psíquico porque enquanto os Pokémon desse tipo dependem principalmente de ataques especiais, os Pokémon do tipo Lutador dependem principalmente de ataques físicos. Em particular, cada ataque do tipo Lutador introduzido antes da Geração IV permaneceu um ataque físico, com ataques especiais de Luta sendo introduzidos apenas a partir da Geração IV.

A maioria dos Pokémon do tipo Lutador tem uma forma de corpo semelhante à humana porque representam praticantes de várias artes marciais, que tendem a ser humanos do mundo real. Alguns Pokémon do tipo Lutador são representados por lutadores (Machamp parece um fisiculturista e Crabrawler parece um lutador francês), enquanto outros são representados por serem baseados em um certo tipo de estilo de luta (Hitmontop é baseado em luta de capoeira e Gallade é baseado em luta de espadas). 

Um número considerável de Pokémon do tipo Fighting são predominantemente ou exclusivamente masculinos. Eles também podem ser representados simplesmente por terem uma força incrível (como Pokémon como Bewear, Pangoro, Crabominable, Conkeldurr e outros têm isso).