---
title: "Ground Type"
date: 2022-10-08T11:44:43-03:00
draft: false
---



Os Pokemon Terra não pode ser Paralisado por movimentos do tipo Elétrico e não recebe dano da condição climática Sandstorm.

Pokémon do tipo Terra têm poderes e habilidades relacionadas ao controle do solo e da terra. Pokémon do tipo solo têm medo de água, como Pokémon do tipo pedra, a menos que sejam do tipo água. Muitos Pokémon terrestres também são parcialmente do tipo Rock.

Esses Pokémon são normalmente encontrados em cavernas ou terrenos rochosos, com exceção de alguns Pokémon de tipo duplo. Uma coisa ruim sobre eles é que eles normalmente não podem sair do chão, como Diglett e Dugtrio, mesmo sendo bons combatentes quando estão abaixo da superfície da Terra. Eles têm movimentos poderosos, como Earthquake e Fissure.