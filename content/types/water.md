---
title: "Water Type"
date: 2022-10-08T12:23:36-03:00
draft: flase
---



Existem mais Pokémon desse tipo do que qualquer outro tipo devido ao grande número de criaturas marinhas das quais basear espécies. A maioria dos Pokémon desse tipo também possui outro tipo, representando a biodiversidade das criaturas marinhas. A água é notavelmente o segundo tipo a ser emparelhado com todos os outros tipos - o Vulcão Fogo/Água completou todos os pares possíveis.

A maioria dos Pokémon do tipo Água são baseados em criaturas que vivem na água ou usam a água para sua disposição. Seus ataques envolvem o uso de água, se não, ataques que podem ser feitos apenas por criaturas marinhas (como Clamp, Crabhammer e Razor Shell).