---
title: "Dark Type"
date: 2021-09-18T23:27:57-03:00
draft: false
---



Os Pokémon Dark são imunes a movimentos que ganham prioridade da Ability Prankster.

O tipo Dark é representado a partir de traços considerados selvagens e indomáveis, especificamente aqueles nascidos de condições sociais complexas adversas e intensas; ao de traços que evocam uma natureza de inclinação negativa - de ter um ciclo diurno noturno, exibições de inteligência extraordinariamente cruel, astuta e inteligente, agressividade beligerante, a consciência senciente e a escolha de prejudicar e/ou infligir má vontade, e ao de o fim negativo da natureza e da realidade; como as sombras, a declinação, "a sobrevivência do mais apto", a destruição, a chegada de tempos mais sombrios; e aos aspectos desconhecidos, mas potencialmente perigosos, do misterioso e enigmático. 

Deve-se notar ainda que o tipo de Dark não se associa automaticamente ao conceito de mal, pois existem evidências de crueldade capaz de Pokémon não apenas do tipo (Ekans, Fearow) e que é o treinador que influencia principalmente como um Pokémon amadurece e age.