---
title: "Ice Type"
date: 2022-10-08T11:44:49-03:00
draft: false
---



Os Pokemon de Gelo não podem ser congelados, são imunes ao movimento Sheer Cold e não recebem dano da condição climática Hail.

Os Pokémon do tipo Gelo se destacam por serem capazes de suportar temperaturas muito baixas, além de se adaptarem a climas congelantes. Eles controlam o gelo à vontade. Seus habitats vão desde o topo das montanhas, cavernas congeladas e cavernas ou até mesmo os pólos.