---
title: "Rock Type"
date: 2022-10-08T11:45:07-03:00
draft: false
---



Os Pokemon Pedra ganham um aumento de 50% na Defesa Especial e não recebem dano da condição climática Sandstorm.

Pokémon do tipo rocha se destacam por sua grande defesa a ataques físicos; no entanto, este tipo está empatado com o tipo Grass como tendo mais fraquezas, com cinco, e os Pokémon deste tipo não são muito rápidos. Além disso, alguns movimentos do tipo Rock não têm grande precisão.

Todos os Pokémon Fossil antes da Geração VIII são deste tipo. Seus fósseis preservam material genético suficiente para ser trabalhado para trazê-los de volta à vida, mas pode-se saber que o tipo Rocha era muito abundante. Eles também possuem combinações únicas para Pokémon do presente.

A maioria dos Pokémon do tipo Rock são organismos biológicos cobertos com armaduras feitas de minerais. No entanto, existem outros Pokémon que se assemelham a rocha viva (como a linha Roggenrola, Nosepass e Minior).

O tipo Rock é comumente combinado com o tipo Ground, então eles geralmente são confundidos, embora não sejam os mesmos. Mesmo no anime, essa confusão esteve presente, dizendo que Pokémon do tipo Rock são imunes a ataques do tipo Elétrico quando esse fator está realmente presente para o tipo Ground.