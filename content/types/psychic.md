---
title: "Psychic Type"
date: 2022-10-08T12:24:10-03:00
draft: false
---



Muitos Pokémon são deste tipo e, sendo o tipo com a maioria dos Pokémon Lendários, para muitos, o tipo Psíquico é o mais poderoso. Pokémon do tipo psíquico tendem a ser muito inteligentes. Também é interessante notar que muitos Pokémon do tipo Psíquico são baseados/relacionados a descobertas científicas ou mitológicas reais, como DNA e Psicocinese.

Para atacar, eles usam seu poder mental, portanto, muitos tendem a ter estatísticas especiais altas e estatísticas físicas baixas. Em particular, todo ataque do tipo Psíquico introduzido antes da Geração IV permaneceu um ataque especial, com ataques psíquicos físicos sendo introduzidos apenas a partir da Geração IV.