---
title: "Steel Type"
date: 2022-10-08T11:45:13-03:00
draft: flase
---



Os Pokemon Aço não podem ser envenenados e não recebem dano da condição climática Sandstorm.

Os Pokémon do tipo aço se destacam por ter uma grande defesa contra ataques físicos e especiais, e um grande número de resistências. No entanto, alguns tipos de aço são capazes de atacar tanto no espectro físico quanto no especial. No entanto, os Pokémon do tipo Steel são pesados ​​e, portanto, têm baixa velocidade.

Enquanto a maioria dos tipos de aço se assemelha a objetos cotidianos feitos de metal (como a linha evolutiva Magnemite, Beldum e Honedge, bem como Ferroseed, Ferrothorn e Klefki), existem alguns tipos de aço que são organismos biológicos equipados com armaduras metálicas ( como Bisharp, Lucario e Steelix).

Pokémon do tipo aço extraem seus poderes de ataques baseados em luz (principalmente ataques especiais) devido às propriedades brilhantes do metal.