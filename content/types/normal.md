---
title: "Normal Type"
date: 2022-10-08T12:23:20-03:00
draft: false
---



Entre as espécies, os Pokémon do tipo Normal tendem a ser baseados em uma variedade de diferentes animais do mundo real. Normalmente, o normal não combinava com outros tipos, exceto o Voador, para retratar espécies padrão de pássaros. À medida que as gerações progrediam, foram introduzidos mais tipos normais que também representavam outros tipos. Em cada geração, alguém encontraria uma espécie puramente do tipo Normal na natureza na primeira rota em que encontrar Pokémon selvagens; essa espécie em particular evoluiria para sua forma final no nível 20 ou antes dele.

O tipo Normal foi especificamente projetado para não ter nenhuma vantagem ofensiva ou defensiva: ofensivamente não atinge nenhum tipo de forma superefetiva, mas dois tipos resistem e um é imune a ele, enquanto defensivamente não resiste a nenhum tipo (mas é imune a um), enquanto um tipo o atinge de forma super eficaz. Pokémon do tipo Normal defensivo normalmente teriam estatísticas defensivas altas, enquanto os tipos Normal ofensivos normalmente aprenderiam uma grande variedade de movimentos.