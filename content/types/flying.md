---
title: "Flying Type"
date: 2022-10-08T11:44:00-03:00
draft: false
---



Os Pokemon Voadores não são considerados aterrados, portanto, não são afetados por coisas como Grassy Terrain e Spikes. Se um tipo Flying usar o movimento Roost, ele perderá o tipo Flying pelo resto do turno.

Pokémon deste tipo podem voar, muitos deles vivem em altitudes elevadas, mesmo. A maioria deles são baseados em pássaros e insetos. Seu poder está principalmente relacionado a movimentos aéreos e relacionados ao vento. 

A maioria deles tem asas, mas também existem alguns que simplesmente flutuam sem asas, como Rayquaza e Gyarados.