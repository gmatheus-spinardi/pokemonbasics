---
title: "Grass Type"
date: 2022-10-08T11:44:23-03:00
draft: false
---



Os Pokemon são imunes aos movimentos Leech Seed, Spore e Powder, bem como ao Ability Effect Spore.

Muitos tipos de grama são baseados em plantas e fungos, não necessariamente grama (Pokémon como Cacturne, que é um cacto, e Foongus, que é um cogumelo). Muitos Pokémon do tipo Grass também pertencem ao Plant Egg Group. Vários tipos de Grama estão emparelhados com o tipo Veneno (Pokémon como Oddish, Venusaur e Bellsprout), refletindo a toxicidade de várias plantas para principalmente humanos. 

A maioria dos tipos de grama também são simplesmente animais com vida vegetal ligada a eles (Pokémon como Bulbasaur, Paras, Sceptile, Gogoat e Tropius). Alguns tipos de grama também são baseados em criaturas míticas (Pokémon como Shiftry, que é baseado em um Tengu, e Tangela, que é baseado em Medusa).