---
title: "Ghost Type"
date: 2022-10-08T11:44:13-03:00
draft: false
---



Os Pokemon Fantasma são imunes a efeitos que impedem a fuga e garantem a fuga de uma batalha selvagem.

Pokémon desse tipo geralmente estão ligados ao medo, à escuridão e à vida após a morte. Costumam viver em casas abandonadas, cemitérios, locais funerários e lugares escuros desabitados, como cavernas. Além disso, Pokémon do tipo Fantasma tendem a ser extremamente travessos, pregando peças em humanos apenas para ver seus rostos e reações. Curiosamente, quanto mais assustado um humano estiver, mais poder o Pokémon Fantasma terá para brincar com ele. 

Na pior das hipóteses, alguns Pokémon do tipo Fantasma podem ter comportamentos relativamente assassinos, como Jellicent atacando navios e suas tripulações ou a família evolucionária de Litwick drenando a energia vital de pessoas que estão perdidas.