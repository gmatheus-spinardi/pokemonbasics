---
title: "Electric Type"
date: 2021-09-18T23:28:40-03:00
draft: false
---



Os Pokemon Eletric não podem ser Paralisados

O tipo Elétrico é um tipo de Pokémon que possui poderes orientados à eletricidade. Eles possuem habilidades eletrocinéticas, sendo capazes de controlar, armazenar ou até produzir eletricidade.

Pokémon do tipo elétrico têm habitats variados, de florestas e pradarias a cidades e usinas de energia. Pokémon do tipo elétrico geralmente são rápidos, e muitos de seus ataques podem paralisar o alvo. Alguns Pokémon do tipo elétrico também se assemelham a objetos artificiais usados ​​por humanos que se relacionam com eletricidade.