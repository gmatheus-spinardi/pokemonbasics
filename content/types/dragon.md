---
title: "Dragon Type"
date: 2022-10-08T12:24:28-03:00
draft: false
layout: "dragon.html"
---



O tipo dragão é frequentemente considerado um tipo ancestral, pois muitos Pokémon lendários do tipo dragão são reverenciados como divindades. Outros Pokémon do tipo Dragão são frequentemente difíceis de capturar e treinar. Eles também evoluem tarde e são relativamente raros. Um fato interessante é que as estatísticas de muitos Pokémon do tipo Dragão superam as estatísticas de outros tipos de Pokémon.

A maioria dos Pokémon com o tipo Dragon são geralmente reptilianos na aparência, com algumas exceções sendo Charizard (que é Fire/Flying por padrão, mas pode se tornar parte Dragon-type se exposto a uma Charizardite X Mega Stone). Outros Pokémon do tipo Dragão não possuem traços dracônicos, como Vibrava (que se assemelha mais a um inseto do que a um dragão) e Alolan Exeggutor (com traços mais próximos de uma planta, mais especificamente de uma palmeira).