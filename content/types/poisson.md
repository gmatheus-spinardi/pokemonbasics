---
title: "Poisson Type"
date: 2022-10-08T11:45:01-03:00
draft: false
---



Os Pokemon Venenoso não pode ser envenenado, e removerá Toxic Spikes de seu lado se aterrado, quando ligado. Se um tipo Poison usar o movimento Toxic, ele não errará.

Esses Pokémon têm uma qualidade tóxica natural; alguns representam diretamente espécies do mundo real conhecidas por seu veneno, como cobras, e alguns até representam a própria poluição. Eles normalmente vivem em cavernas, pântanos ou lugares semelhantes.

A maioria dos Pokémon do tipo Veneno são baseados em animais venenosos ou venenosos (Pokémon como Seviper, que é uma cobra, Drapion, que é um escorpião e Scolipede, que é uma centopéia). Alguns deles também são baseados em plantas venenosas ou fungos (Pokémon como Foongus, que é um cogumelo, e Oddish, que é uma raiz de mandrágora). A maioria dos Pokémon do tipo Veneno duplo tem o tipo Inseto ou o tipo Grama como seu outro tipo. Isso reflete como, na vida real, muitos insetos e plantas são venenosos ou venenosos. 

Um fato muito curioso sobre os Pokémon do tipo Veneno é que alguns deles se assemelham a ninjas. Por exemplo, Koffing é como uma bola de fumaça, Crobat tem a velocidade e sigilo de um ninja e Toxicroak tem patas que o fazem parecer um ninja. 

Alguns tipos de veneno também se assemelham a tipos de poluição. Por exemplo, Garbodor representa a poluição do lixo, Weezing representa a poluição do ar e Muk representa a poluição líquida. Estranhamente, nem todos os tipos de veneno são baseados em coisas venenosas ou venenosas (Stunky, que é baseado em um gambá, e Skrelp, que é baseado em um dragão marinho folhoso, são ambos animais da vida real que não são venenosos ou venenosos).