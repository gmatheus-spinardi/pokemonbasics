---
title: "Fairy Type"
date: 2022-10-08T12:18:13-03:00
draft: false
---


Os Pokemon Fada foram os últimos a serem adicionados nos jogos da franquia. Muitos Pokemons foram mudados para este tipo após a Geração 6.

Pokémon do tipo fada são geralmente considerados "fofos" e "elegantes" e tendem a ser pelo menos parcialmente rosa na cor e na aparência feminina em geral (Pokémon como Igglybuff e Swirlix têm essas características). No entanto, os tipos Fairy podem ser incrivelmente poderosos. Os tipos fada também têm algum apelo mágico para eles. Ao contrário dos Pokémon do tipo Psíquico, os Pokémon do tipo Fada têm poderes relacionados à magia e ao sobrenatural, em oposição ao poder mental. Os tipos de fadas também são conhecidos por seus poderes de pureza e rejuvenescimento (Pokémon como Gardevoir, Xerneas e Galarian Weezing são mostrados como tendo poderes especiais que podem purificar objetos). 

Outros tipos de Fadas com poderes não psíquicos ainda recebem o tipo Fada para certas habilidades mágicas (Jigglypuff pode colocar as pessoas para dormir cantando e os guardiões Tapu são vistos como deuses). Eles também podem ser baseados em outros tipos de poderes, ter origens misteriosas, ou ser baseados em criaturas de contos populares (Pokémon como Clefairy, que vem da lua, e Diancie, que é uma forma mutante de Carbink). 

Muitos tipos de fadas também são conhecidos por sua natureza quase doce, metaforicamente e literalmente (Pokémon como Spritzee, Slurpuff e Flabébé são geralmente baseados em coisas de cheiro doce como perfume, doces ou flores). No entanto, outros são mais perigosos e traiçoeiros (como Mawile e suas enormes mandíbulas; Impidimp, que se alimenta de emoções de pessoas frustradas; ou Shiinotic, que atrai presas com luzes), que são mais condizentes com as fadas vistas no folclore.