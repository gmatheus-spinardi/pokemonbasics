---
title: "Pikachu"
date: 2022-09-26T15:12:59-03:00
draft: false
layout: "pikachu.html"
---

Sendo basicamente a cara da franquia, o Pikachu é o Pokemon mais famoso por todo mundo. Arrisco dizer que há mais pessoas que sabem sobre o Pikachu do que até mesmo sobre Pokemon em si.

Aparecendo no anime desde o primeiro episódio, o Pikachu nunca deixou de aparer em qualquer episódio da série principal ou em qualquer filme em que aparecesse o Ash como protagonista.

Sendo dado ao Ash pelo Professor Carvalho no primeiro episódio, o Pikachu sempre se recusou a entrar na Pokebola, nunca foi dado uma explicação do porque ele não gosta de ficar lá dentro, mas como 
consequência temos o Pikachu aparecendo quase que o tempo todo na tela.

Um fato interessante é que durante os jogos, o Pikachu não tem um papel de protagonista, durante a sua jogatina, e na verdade é considerado um Pokemon normal como qualquer outro. A única exceção é o jogo "Pokemon Yellow" que é uma versão aprimorada do "Pokemon Red e Blue" que tem apenas o Pikachu como opção de Pokemon inicial.